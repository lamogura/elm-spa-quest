module Route exposing (Route(..), fromLocation)

import UrlParser as Url exposing (parseHash, s, (</>), string, oneOf, Parser)
import Navigation exposing (Location)
import Debug exposing (log)


-- ROUTING --


type Route
    = Home


route : Parser (Route -> a) a
route =
    oneOf [ Url.map Home (s "") ]



-- PUBLIC HELPERS --


fromLocation : Location -> Maybe Route
fromLocation location =
    if String.isEmpty location.hash then
        Just Home
    else
        parseHash route (log "got location: " location)
