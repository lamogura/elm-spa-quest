module Request.Article.Feed exposing (global)

import Http
import HttpBuilder
import Data.Article.Feed exposing (Feed, feedDecoder)
import Request.Helpers exposing (apiUrl)


global : Http.Request Feed
global =
    apiUrl "/articles"
        |> HttpBuilder.get
        |> HttpBuilder.withExpect (Http.expectJson feedDecoder)
        |> HttpBuilder.toRequest
