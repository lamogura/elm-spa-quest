module Page.Errored exposing (PageLoadError, pageLoadError, view)

import Html exposing (..)
import Html.Attributes exposing (..)


type alias State =
    { errorMessage : String
    }


type PageLoadError
    = PageLoadError State


pageLoadError : String -> PageLoadError
pageLoadError errorMessage =
    PageLoadError { errorMessage = errorMessage }


view : PageLoadError -> Html msg
view (PageLoadError { errorMessage }) =
    div [ class "columns" ]
        [ div [ class "column is-half is-offset-one-quarter" ]
            [ div [ class "message is-danger" ]
                [ div [ class "message-header" ]
                    [ p [] [ text "Error Loading Page" ]
                    ]
                , div [ class "message-body" ] [ text errorMessage ]
                ]
            ]
        ]
