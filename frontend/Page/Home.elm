module Page.Home exposing (..)

import Http
import Html exposing (..)
import Html.Attributes exposing (..)
import Request.Article.Feed
import Data.Article.Feed exposing (Feed)
import Views.Article exposing (viewFeed)
import Page.Errored as Errored exposing (PageLoadError, pageLoadError)
import Util exposing ((=>))
import Task exposing (Task)


-- STATE --


type alias State =
    { feed : Feed
    }


init : Task PageLoadError State
init =
    let
        loadFeed =
            Http.toTask Request.Article.Feed.global

        handleLoadError _ =
            pageLoadError "Homepage is currently unavailable."
    in
        Task.map State loadFeed
            |> Task.mapError handleLoadError



-- VIEW --


view : State -> Html msg
view state =
    div [] [ viewHero, viewFeed state.feed ]


viewHero : Html msg
viewHero =
    div [ class "hero is-primary has-text-centered has-shadow" ]
        [ div [ class "hero-body" ]
            [ div [ class "title" ] [ text "conduit" ]
            , div [ class "subtitle" ] [ text "A place to share your knowledge." ]
            ]
        ]



-- UPDATE --


type Msg
    = FeedLoaded Feed


update : Msg -> State -> ( State, Cmd Msg )
update msg state =
    case msg of
        FeedLoaded newFeed ->
            { state | feed = newFeed } => Cmd.none
