module Data.Article.Feed exposing (Feed, feedDecoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline exposing (decode, required, optionalAt)
import Data.Article exposing (Article, articleDecoder)


type alias Feed =
    { articles : List Article
    , articlesCount : Int
    }


feedDecoder : Decoder Feed
feedDecoder =
    decode Feed
        |> required "articles" (Decode.list articleDecoder)
        |> required "articlesCount" Decode.int
