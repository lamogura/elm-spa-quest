module Data.Article exposing (..)

import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Pipeline exposing (decode, required, optionalAt)
import Json.Decode.Extra


type alias Article =
    { title : String
    , description : String
    , favoritesCount : Int
    , author : String
    , createdAt : Date
    }


articleDecoder : Decoder Article
articleDecoder =
    decode Article
        |> required "title" Decode.string
        |> required "description" Decode.string
        |> required "favoritesCount" Decode.int
        |> optionalAt [ "author", "username" ] Decode.string "Anonymous"
        |> required "createdAt" Json.Decode.Extra.date
