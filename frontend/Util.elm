module Util exposing (..)

import Html exposing (Html, text)


(=>) : a -> b -> ( a, b )
(=>) =
    (,)


viewIf : Bool -> Html msg -> Html msg
viewIf condition content =
    if condition then
        content
    else
        text ""
