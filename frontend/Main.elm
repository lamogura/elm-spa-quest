module Main exposing (..)

import Navigation exposing (Location)
import Route exposing (Route)
import Task
import Html exposing (..)
import Html.Attributes exposing (..)
import Views.Layout as Layout
import Page.Home as Home
import Page.Errored as Errored exposing (PageLoadError)
import Util exposing ((=>))


type Msg
    = SetRoute (Maybe Route)
    | HomeLoaded (Result PageLoadError Home.State)
    | HomeMsg Home.Msg


type Page
    = Blank
    | NotFound
    | Home Home.State
    | Errored PageLoadError


type PageState
    = Loaded Page
    | TransitioningFrom Page



-- STATE --


type alias State =
    { pageState : PageState
    }


initialPage : Page
initialPage =
    Blank


init : Location -> ( State, Cmd Msg )
init location =
    setRoute (Route.fromLocation location)
        { pageState = Loaded initialPage }


getPage : PageState -> Page
getPage pageState =
    case pageState of
        TransitioningFrom page ->
            page

        Loaded page ->
            page


setRoute : Maybe Route -> State -> ( State, Cmd Msg )
setRoute maybeRoute state =
    let
        transition toMsg task =
            { state | pageState = TransitioningFrom (getPage state.pageState) }
                => Task.attempt toMsg task
    in
        case maybeRoute of
            Nothing ->
                { state | pageState = Loaded NotFound } => Cmd.none

            Just Route.Home ->
                transition HomeLoaded Home.init



-- VIEW --


view : State -> Html Msg
view state =
    case state.pageState of
        Loaded page ->
            viewPage False page

        TransitioningFrom page ->
            viewPage True page


viewNotImplemented : Page -> Html Msg
viewNotImplemented page =
    div [ class "columns" ]
        [ div [ class "column is-half is-offset-one-quarter" ]
            [ div [ class "message" ]
                [ div [ class "message-body" ]
                    [ text ("Page Not Yet Implemented: " ++ (toString page))
                    ]
                ]
            ]
        ]


viewPage : Bool -> Page -> Html Msg
viewPage isLoading page =
    let
        layout =
            Layout.view isLoading
    in
        case page of
            Home subState ->
                Home.view subState
                    |> layout
                    |> Html.map HomeMsg

            Errored subState ->
                Errored.view subState
                    |> layout

            otherPage ->
                viewNotImplemented initialPage
                    |> layout



-- UPDATE --


update : Msg -> State -> ( State, Cmd Msg )
update msg state =
    let
        currentPage =
            getPage state.pageState

        updatePage page pageMsg subUpdate subMsg subState =
            let
                ( newSubState, newCmd ) =
                    subUpdate subMsg subState
            in
                ( { state | pageState = Loaded (page newSubState) }, Cmd.map pageMsg newCmd )
    in
        case ( msg, currentPage ) of
            ( SetRoute route, _ ) ->
                setRoute route state

            ( HomeLoaded (Ok subState), _ ) ->
                { state | pageState = Loaded (Home subState) } => Cmd.none

            ( HomeLoaded (Err error), _ ) ->
                { state | pageState = Loaded (Errored error) } => Cmd.none

            ( HomeMsg subMsg, Home subState ) ->
                updatePage Home HomeMsg Home.update subMsg subState

            ( _, NotFound ) ->
                -- Disregard incoming messages when we're on the NotFound page.
                state => Cmd.none

            ( _, _ ) ->
                -- Disregard incoming messages that arrived for the wrong page
                state => Cmd.none



-- MAIN --


main : Program Never State Msg
main =
    Navigation.program (Route.fromLocation >> SetRoute)
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }
