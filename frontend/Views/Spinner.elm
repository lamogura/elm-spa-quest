module Views.Spinner exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Util exposing ((=>))


view : Html msg
view =
    span [ class "nav-icon", style [ "margin-top" => "16px" ] ]
        [ i [ class "fa fa-spinner fa-spin" ] []
        ]
