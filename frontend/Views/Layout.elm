module Views.Layout exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Views.Spinner as Spinner
import Util exposing (viewIf)


view : Bool -> Html msg -> Html msg
view isLoading content =
    div []
        [ viewNavbar isLoading
        , content
        ]


viewNavbar : Bool -> Html msg
viewNavbar isLoading =
    div [ class "nav" ]
        [ div [ class "container" ]
            [ div [ class "nav-left" ]
                [ a [ class "nav-item nav-brand" ]
                    [ text "conduit" ]
                ]
            , div [ class "nav-right" ]
                [ viewIf isLoading Spinner.view
                , a [ class "nav-item is-active" ]
                    [ text "Home" ]
                , a [ class "nav-item" ]
                    [ text "Sign in" ]
                , a [ class "nav-item" ]
                    [ text "Sign up" ]
                ]
            ]
        ]
