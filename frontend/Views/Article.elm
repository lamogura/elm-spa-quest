module Views.Article exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, style, href, src, id, placeholder, attribute, classList)
import Date exposing (Date)
import Date.Format
import Data.Article exposing (Article)
import Data.Article.Feed exposing (Feed)
import Util exposing ((=>))
import Debug exposing (log)


viewArticle : Article -> Html msg
viewArticle { title, author, description, createdAt, favoritesCount } =
    div [ class "card" ]
        [ div [ class "card-content", style [ "margin-bottom" => "20px" ] ]
            [ div [ class "media" ]
                [ div [ class "media-left" ]
                    [ figure [ class "avatar image is-48x48" ]
                        [ img [ src "https://static.productionready.io/images/smiley-cyrus.jpg" ] []
                        ]
                    ]
                , div [ class "media-content" ]
                    [ p [ class "title is-4" ] [ text author ]
                    , p [ class "subtitle is-6" ] [ text (formattedTimestamp createdAt) ]
                    ]
                , div [ class "media-right" ]
                    [ a [ class "button is-small is-primary is-outlined" ]
                        [ faIcon "heart"
                        , p [ style [ "font-size" => "1.1rem" ] ]
                            [ text (toString favoritesCount)
                            ]
                        ]
                    ]
                ]
            , div [ class "content" ]
                [ div [ class "title" ] [ text title ]
                , div [ class "subtitle" ] [ text description ]
                ]
            ]
        ]


viewFeed : Feed -> Html msg
viewFeed feed =
    div
        [ class "container"
        , style [ "padding-top" => "40px", "padding-bottom" => "40px" ]
        ]
        [ div [ class "tabs" ]
            [ ul []
                [ li [ class "is-active" ]
                    [ a [] [ text "Global Feed" ]
                    ]
                ]
            ]
        , div
            [ class "container" ]
            (List.map viewArticle (log "articles" feed.articles))
        ]



-- INTERNAL --


faIcon : String -> Html msg
faIcon name =
    span [ class "icon is-small" ]
        [ i [ class ("fa fa-" ++ name) ] []
        ]


formattedTimestamp : Date -> String
formattedTimestamp date =
    Date.Format.format "%B %e, %Y" date
